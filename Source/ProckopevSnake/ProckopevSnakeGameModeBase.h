// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProckopevSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROCKOPEVSNAKE_API AProckopevSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
